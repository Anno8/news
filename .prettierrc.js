module.exports = {
  semi: true,
  singleQuote: false,
  jsxSingleQuote: false,
  jsxBracketSameLine: false,
  trailingComma: "none",
  endOfLine: "auto"
};
