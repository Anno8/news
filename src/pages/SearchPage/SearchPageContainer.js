import { connect } from "react-redux";

import { fetchAllTopNews } from "modules/News/NewsActions";
import {
  getAllTopNews,
  getAllTopNewsLoading,
  getAllTopNewsError
} from "modules/News/NewsSelectors";

import { getSelectedCountry } from "modules/Country/CountrySelectors";

import SearchPage from "./SearchPage";

export const mapStateToProps = (state) => ({
  news: getAllTopNews(state),
  loading: getAllTopNewsLoading(state),
  error: getAllTopNewsError(state),
  country: getSelectedCountry(state)
});

export default connect(mapStateToProps, { fetchAllTopNews })(SearchPage);
