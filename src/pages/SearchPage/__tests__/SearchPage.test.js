import { shallow } from "enzyme";
import React from "react";

import SearchPage from "../SearchPage";

describe("SearchPage tests", () => {
  const mockedProps = {
    fetchAllTopNews: jest.fn(),
    news: [
      {
        title: "News 1"
      },
      {
        title: "News 2"
      }
    ],
    error: null,
    loading: false,
    country: {
      code: "US",
      name: "United States"
    }
  };
  it("Should match snapshot", () => {
    const wrapper = shallow(<SearchPage {...mockedProps} />);

    expect(wrapper).toMatchSnapshot();
  });

  it("Should match snapshot when loading", () => {
    const wrapper = shallow(<SearchPage {...mockedProps} loading />);

    expect(wrapper).toMatchSnapshot();
  });

  it("Should match snapshot when an error ocurred", () => {
    const wrapper = shallow(
      <SearchPage {...mockedProps} error="An error happened" />
    );

    expect(wrapper).toMatchSnapshot();
  });
});
