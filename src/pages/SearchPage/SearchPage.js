import React, { useState, useEffect, useCallback } from "react";
import { debounce } from "lodash";

import Loader from "components/Loader";
import NewsCard from "components/NewsCard";

import styles from "./SearchPage.module.css";

export const SearchPage = ({
  fetchAllTopNews,
  news,
  loading,
  error,
  country
}) => {
  const [searchCriteria, setSearchCriteria] = useState("");
  useEffect(() => {
    fetchAllTopNews(searchCriteria);
    // searchCriteria isn't added as a dependency since I am handling it with debounce
    // as not to spam requests on every change
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [country.code, fetchAllTopNews]);

  const handleSearchCriteriaChange = (searchCriteria) => {
    setSearchCriteria(searchCriteria);
    debouncedSearch(searchCriteria);
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const debouncedSearch = useCallback(
    debounce((searchCriteria) => fetchAllTopNews(searchCriteria), 800),
    []
  );

  return (
    <>
      <h1> Search top news from country {country.name} by term:</h1>
      <div>
        <input
          className={styles.searchField}
          type="text"
          placeholder="Search term"
          value={searchCriteria}
          onChange={(e) => handleSearchCriteriaChange(e.target.value)}
        ></input>
      </div>
      {loading && <Loader />}
      {!loading && error && <div>{error}</div>}
      <div className={styles.newsList}>
        {!loading &&
          !error &&
          news.map((n) => <NewsCard key={n.title} news={n} />)}
      </div>
    </>
  );
};

export default SearchPage;
