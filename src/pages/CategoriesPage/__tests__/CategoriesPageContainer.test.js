import { mapStateToProps } from "../CategoriesPageContainer";

jest.mock("modules/Country/CountrySelectors", () => ({
  getSelectedCountry: jest.fn(() => "selected country")
}));

jest.mock("modules/News/NewsSelectors", () => ({
  getAllNewsCategories: jest.fn(() => ["News1", "News2"])
}));
describe("CategoriesPageContainer tests", () => {
  it("Should be a function", () => {
    expect(typeof mapStateToProps).toEqual("function");
  });

  it("should return an object", () => {
    expect(typeof mapStateToProps({})).toEqual("object");
  });

  it("should have property values", () => {
    const props = mapStateToProps({});
    expect(props).toHaveProperty("categories");
    expect(props).toHaveProperty("country");
  });
});
