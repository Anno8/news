import { shallow } from "enzyme";
import React from "react";

import CategoriesPage from "../CategoriesPage";

describe("CategoriesPage tests", () => {
  const mockedProps = {
    categories: ["business", "sport"],
    country: {
      code: "US",
      name: "United States"
    }
  };
  it("Should match snapshot", () => {
    const wrapper = shallow(<CategoriesPage {...mockedProps} />);

    expect(wrapper).toMatchSnapshot();
  });
});
