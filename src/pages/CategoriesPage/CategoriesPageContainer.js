import { connect } from "react-redux";

import { getAllNewsCategories } from "modules/News/NewsSelectors";
import { fetchTopNewsByCategory } from "modules/News/NewsActions";
import { getSelectedCountry } from "modules/Country/CountrySelectors";

import CategoriesPage from "./CategoriesPage";

export const mapStateToProps = (state) => ({
  categories: getAllNewsCategories(state),
  country: getSelectedCountry(state)
});

export default connect(mapStateToProps, { fetchTopNewsByCategory })(
  CategoriesPage
);
