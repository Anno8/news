import React from "react";

import CategoriesDisplay from "components/CategoriesDisplay";

export const CategoriesPage = ({ categories, country }) => (
  <div>
    <h2>Top 5 news by categories from {country.name}</h2>
    {categories.map((c) => (
      <CategoriesDisplay category={c} key={c} />
    ))}
  </div>
);

export default CategoriesPage;
