import { shallow } from "enzyme";
import React from "react";

import TopNewsPage from "../TopNewsPage";

describe("TopNewsPage tests", () => {
  const mockedProps = {
    fetchAllTopNews: jest.fn(),
    news: [
      {
        title: "News 1"
      },
      {
        title: "News 2"
      }
    ],
    error: null,
    loading: false,
    country: {
      code: "US",
      name: "United States"
    }
  };
  it("Should match snapshot", () => {
    const wrapper = shallow(<TopNewsPage {...mockedProps} />);

    expect(wrapper).toMatchSnapshot();
  });

  it("Should match snapshot when loading", () => {
    const wrapper = shallow(<TopNewsPage {...mockedProps} loading />);

    expect(wrapper).toMatchSnapshot();
  });

  it("Should match snapshot when an error ocurred", () => {
    const wrapper = shallow(
      <TopNewsPage {...mockedProps} error="An error happened" />
    );

    expect(wrapper).toMatchSnapshot();
  });
});
