import { mapStateToProps } from "../TopNewsPageContainer";

jest.mock("modules/Country/CountrySelectors", () => ({
  getSelectedCountry: jest.fn(() => "selected country")
}));

jest.mock("modules/News/NewsSelectors", () => ({
  getAllTopNews: jest.fn(() => ["News1", "News2"]),
  getAllTopNewsError: jest.fn(() => null),
  getAllTopNewsLoading: jest.fn(() => false)
}));
describe("TopNewsPageContainer tests", () => {
  it("Should be a function", () => {
    expect(typeof mapStateToProps).toEqual("function");
  });

  it("should return an object", () => {
    expect(typeof mapStateToProps({})).toEqual("object");
  });

  it("should have property values", () => {
    const props = mapStateToProps({});
    expect(props).toHaveProperty("news");
    expect(props).toHaveProperty("error");
    expect(props).toHaveProperty("loading");
    expect(props).toHaveProperty("country");
  });
});
