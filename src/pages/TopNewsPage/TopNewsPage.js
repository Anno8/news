import React, { useEffect } from "react";

import NewsCard from "components/NewsCard";
import Loader from "components/Loader";

import styles from "./TopNewsPage.module.css";

export const TopNewsPage = ({
  fetchAllTopNews,
  news,
  error,
  loading,
  country
}) => {
  useEffect(() => {
    fetchAllTopNews();
  }, [fetchAllTopNews, country]);
  if (loading) {
    return <Loader />;
  }

  if (!loading && error) {
    return <div>{error}</div>;
  }

  return (
    <>
      <h1>Top news from {country.name}:</h1>
      <div className={styles.newsList}>
        {news.map((n) => (
          <NewsCard key={n.title} news={n} />
        ))}
      </div>
    </>
  );
};
export default TopNewsPage;
