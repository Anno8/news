import { connect } from "react-redux";

import { fetchAllTopNews } from "modules/News/NewsActions";
import {
  getAllTopNews,
  getAllTopNewsError,
  getAllTopNewsLoading
} from "modules/News/NewsSelectors";
import { getSelectedCountry } from "modules/Country/CountrySelectors";

import TopNewsPage from "./TopNewsPage";

export const mapStateToProps = (state) => ({
  news: getAllTopNews(state),
  error: getAllTopNewsError(state),
  loading: getAllTopNewsLoading(state),
  country: getSelectedCountry(state)
});

export default connect(mapStateToProps, { fetchAllTopNews })(TopNewsPage);
