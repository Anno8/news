import React from "react";
import { withRouter } from "react-router-dom";

export const NotFoundPage = ({ location: { pathname } }) => (
  <h3>Page not found url: {pathname}</h3>
);

export default withRouter(NotFoundPage);
