import { shallow } from "enzyme";
import React from "react";

import { NewsDetailsPage } from "../NewsDetailsPage";

describe("NewsDetailsPage page tests", () => {
  const mockedProps = {
    history: {
      push: jest.fn(),
      goBack: jest.fn(),
      location: {
        state: {
          title: "test title",
          description: "test description",
          urlToImage: "test urlToImage",
          content: "test content"
        }
      }
    }
  };

  it("Should match snapshot", () => {
    const wrapper = shallow(<NewsDetailsPage {...mockedProps} />);

    expect(wrapper).toMatchSnapshot();
  });

  it("Should navigate back", () => {
    const wrapper = shallow(<NewsDetailsPage {...mockedProps} />);
    const more = wrapper.find(".link");

    more.simulate("click");

    expect(mockedProps.history.goBack).toHaveBeenCalled();
  });
});
