import React from "react";
import { withRouter } from "react-router-dom";

import styles from "./NewsDetailsPage.module.css";

export const NewsDetailsPage = ({ history }) => {
  // Since I couldn't find a get by id endpoint, passing the retrieved data through navigation
  const data = history?.location?.state;

  const { title, content, urlToImage } = data;
  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <h1>{title}</h1>
        <img alt="not found" src={urlToImage}></img>
        <p>{content}</p>
        <p className="link" onClick={history.goBack}>
          &lt; Back to list
        </p>
      </div>
    </div>
  );
};

export default withRouter(NewsDetailsPage);
