import { REDUCER_KEY } from "./CountryConstants";

export const getCountries = (state) => state[REDUCER_KEY]?.countries;
export const getSelectedCountry = (state) =>
  state[REDUCER_KEY]?.selectedCountry;
