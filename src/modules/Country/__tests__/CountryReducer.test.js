import CountryReducer from "../CountryReducer";
import { COUNTRY_CHANGED } from "../CountryConstants";

describe("CountryReducer tests", () => {
  it("Should be a function", () => {
    expect(typeof CountryReducer).toBe("function");
  });

  it("Should return an object", () => {
    expect(typeof CountryReducer({}, {})).toBe("object");
  });

  it("Should return selected country", () => {
    // Arrange
    const mockedState = {
      countries: [
        {
          code: "GB",
          name: "Great Britain"
        },
        {
          code: "US",
          name: "United States"
        }
      ],
      selectedCountry: {
        code: "GB",
        name: "Great Britain"
      }
    };

    const newSelectedCountry = {
      code: "US",
      name: "United States"
    };

    const action = { type: COUNTRY_CHANGED, payload: newSelectedCountry };

    // Act
    const result = CountryReducer(mockedState, action);

    // Assert
    expect(result).toEqual({
      ...mockedState,
      selectedCountry: newSelectedCountry
    });
  });
});
