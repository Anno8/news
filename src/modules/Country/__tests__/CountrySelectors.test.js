import { getCountries, getSelectedCountry } from "../CountrySelectors";
import { REDUCER_KEY } from "../CountryConstants";

describe("CountrySelectors tests", () => {
  describe("getCountries select tests", () => {
    it("Should be a function", () => {
      expect(typeof getCountries).toEqual("function");
    });

    it("should return countries from state", () => {
      const mockedData = [
        {
          code: "GB",
          name: "Great Britain"
        },
        {
          code: "US",
          name: "United States"
        }
      ];

      const state = {
        [REDUCER_KEY]: {
          countries: mockedData
        }
      };

      const result = getCountries(state);

      expect(result).toEqual(mockedData);
    });
  });

  describe("getSelectedCountry select tests", () => {
    it("Should be a function", () => {
      expect(typeof getSelectedCountry).toEqual("function");
    });

    it("should return selected country from state", () => {
      const mockedData = {
        code: "GB",
        name: "Great Britain"
      };

      const state = {
        [REDUCER_KEY]: {
          selectedCountry: mockedData
        }
      };

      const result = getSelectedCountry(state);

      expect(result).toEqual(mockedData);
    });
  });
});
