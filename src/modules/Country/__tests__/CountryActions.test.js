import { changeCountry } from "../CountryActions";
import { COUNTRY_CHANGED } from "../CountryConstants";

describe("CountryActions tests", () => {
  it("Should dispatch COUNTRY_CHANGED ", () => {
    // Arrange
    const dispatch = jest.fn();
    const country = {
      name: "Full test name country",
      code: "code"
    };
    // Act
    changeCountry(country)(dispatch);

    // Assert
    expect(dispatch).toHaveBeenCalledWith({
      type: COUNTRY_CHANGED,
      payload: country
    });
  });
});
