export const REDUCER_KEY = "country";
export const COUNTRY_CHANGED = `${REDUCER_KEY}.country.changed`;

export const INIT_STATE = {
  countries: [
    {
      code: "GB",
      name: "Great Britain"
    },
    {
      code: "US",
      name: "United States"
    }
  ],
  selectedCountry: {
    code: "GB",
    name: "Great Britain"
  }
};
