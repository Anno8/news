import { INIT_STATE, COUNTRY_CHANGED } from "./CountryConstants";

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case COUNTRY_CHANGED: {
      return {
        ...state,
        selectedCountry: action.payload
      };
    }
    default:
      return { ...state };
  }
};
