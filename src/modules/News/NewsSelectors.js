import { REDUCER_KEY } from "./NewsConstants";

export const getAllTopNews = (state) => state[REDUCER_KEY]?.topNews?.news;
export const getAllTopNewsError = (state) => state[REDUCER_KEY]?.topNews?.error;
export const getAllTopNewsLoading = (state) =>
  state[REDUCER_KEY]?.topNews?.loading;

export const getAllNewsCategories = (state) =>
  Object.keys(state[REDUCER_KEY]?.newsPerCategory);
export const getNewsByCategory = (state, category) =>
  state[REDUCER_KEY]?.newsPerCategory[category]?.news;
export const getNewsPerCategoryError = (state, category) =>
  state[REDUCER_KEY]?.newsPerCategory[category]?.error;
export const getNewsPerCategoryLoading = (state, category) =>
  state[REDUCER_KEY]?.newsPerCategory[category]?.loading;
