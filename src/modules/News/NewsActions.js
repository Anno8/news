import { apiCall } from "utils/ApiCall";
import { getSelectedCountry } from "modules/Country/CountrySelectors";

import {
  FETCHING_TOP_NEWS,
  FETCHING_TOP_NEWS_SUCCESS,
  FETCHING_TOP_NEWS_FAILURE,
  FETCHING_TOP_NEWS_PER_CATEGORY_SUCCESS,
  FETCHING_TOP_NEWS_PER_CATEGORY,
  FETCHING_TOP_NEWS_PER_CATEGORY_FAILURE
} from "./NewsConstants";

const fetchingTopNewsActionCreator = () => ({
  type: FETCHING_TOP_NEWS
});

const fetchingTopNewsSuccessActionCreator = (news) => ({
  type: FETCHING_TOP_NEWS_SUCCESS,
  payload: news
});

const fetchingTopNewsFailedActionCreator = (error) => ({
  type: FETCHING_TOP_NEWS_FAILURE,
  payload: error
});

const fetchingTopNewsPerCategoryActionCreator = (category) => ({
  type: FETCHING_TOP_NEWS_PER_CATEGORY,
  payload: category
});

const fetchingTopNewsPerCategorySuccessActionCreator = (news, category) => ({
  type: FETCHING_TOP_NEWS_PER_CATEGORY_SUCCESS,
  payload: { news, category }
});

const fetchingTopNewsPerCategoryFailedActionCreator = (category, error) => ({
  type: FETCHING_TOP_NEWS_PER_CATEGORY_FAILURE,
  payload: {
    category,
    error
  }
});

export const fetchAllTopNews = (searchCriteria = "") => async (
  dispatch,
  getState
) => {
  dispatch(fetchingTopNewsActionCreator());
  const selectedCountry = getSelectedCountry(getState());
  const url = `top-headlines?country=${selectedCountry.code}&q=${searchCriteria}`;
  try {
    const { articles, status, message } = await apiCall(url);
    if (status === "ok") {
      dispatch(fetchingTopNewsSuccessActionCreator(articles));
    } else {
      dispatch(fetchingTopNewsFailedActionCreator(message));
    }
  } catch (e) {
    dispatch(fetchingTopNewsFailedActionCreator(e.message));
  }
};

export const fetchTopNewsByCategory = (category, pageSize) => async (
  dispatch,
  getState
) => {
  dispatch(fetchingTopNewsPerCategoryActionCreator(category));

  const selectedCountry = getSelectedCountry(getState());
  const url = `top-headlines?country=${selectedCountry.code}&category=${category}&pageSize=${pageSize}`;

  try {
    const { articles, status, message } = await apiCall(url);
    if (status === "ok") {
      dispatch(
        fetchingTopNewsPerCategorySuccessActionCreator(articles, category)
      );
    } else {
      dispatch(
        fetchingTopNewsPerCategoryFailedActionCreator(category, message)
      );
    }
  } catch (e) {
    dispatch(
      fetchingTopNewsPerCategoryFailedActionCreator(category, e.message)
    );
  }
};
