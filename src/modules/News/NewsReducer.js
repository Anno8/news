import {
  INIT_STATE,
  FETCHING_TOP_NEWS_FAILURE,
  FETCHING_TOP_NEWS_SUCCESS,
  FETCHING_TOP_NEWS,
  FETCHING_TOP_NEWS_PER_CATEGORY,
  FETCHING_TOP_NEWS_PER_CATEGORY_SUCCESS,
  FETCHING_TOP_NEWS_PER_CATEGORY_FAILURE
} from "./NewsConstants";

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case FETCHING_TOP_NEWS: {
      return {
        ...state,
        topNews: {
          loading: true
        }
      };
    }
    case FETCHING_TOP_NEWS_SUCCESS: {
      return {
        ...state,
        topNews: {
          news: action.payload,
          loading: false
        }
      };
    }
    case FETCHING_TOP_NEWS_FAILURE: {
      return {
        ...state,
        topNews: {
          error: action.payload,
          loading: false
        }
      };
    }
    case FETCHING_TOP_NEWS_PER_CATEGORY: {
      return {
        ...state,
        newsPerCategory: {
          ...state.newsPerCategory,
          [action.payload]: {
            loading: true
          }
        }
      };
    }
    case FETCHING_TOP_NEWS_PER_CATEGORY_SUCCESS: {
      return {
        ...state,
        newsPerCategory: {
          ...state.newsPerCategory,
          [action.payload.category]: {
            news: action.payload.news,
            loading: false
          }
        }
      };
    }
    case FETCHING_TOP_NEWS_PER_CATEGORY_FAILURE: {
      return {
        ...state,
        newsPerCategory: {
          ...state.newsPerCategory,
          [action.payload.category]: {
            loading: false,
            error: action.payload.error
          }
        }
      };
    }
    default:
      return { ...state };
  }
};
