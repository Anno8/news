export const REDUCER_KEY = "news";
export const FETCHING_TOP_NEWS = `${REDUCER_KEY}.fetching.top.news`;
export const FETCHING_TOP_NEWS_SUCCESS = `${REDUCER_KEY}.fetching.top.news.success`;
export const FETCHING_TOP_NEWS_FAILURE = `${REDUCER_KEY}.fetching.top.news.failure`;
export const FETCHING_TOP_NEWS_PER_CATEGORY = `${REDUCER_KEY}.fetching.top.news.per.category`;
export const FETCHING_TOP_NEWS_PER_CATEGORY_SUCCESS = `${REDUCER_KEY}.fetching.top.news.per.category.success`;
export const FETCHING_TOP_NEWS_PER_CATEGORY_FAILURE = `${REDUCER_KEY}.fetching.top.news.per.category.failure`;

const initNewsState = {
  news: [],
  error: null,
  loading: true
};

// Split into two reducers ?
export const INIT_STATE = {
  topNews: {
    ...initNewsState
  },
  // Hardcoded categories defined on newsapi.org
  newsPerCategory: {
    business: {
      ...initNewsState
    },
    entertainment: {
      ...initNewsState
    },
    general: {
      ...initNewsState
    },
    health: {
      ...initNewsState
    },
    science: {
      ...initNewsState
    },
    sports: {
      ...initNewsState
    },
    technology: {
      ...initNewsState
    }
  }
};
