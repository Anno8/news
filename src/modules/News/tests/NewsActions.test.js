import { apiCall } from "utils/ApiCall";

import { REDUCER_KEY } from "modules/Country/CountryConstants";

import { fetchAllTopNews, fetchTopNewsByCategory } from "../NewsActions";
import {
  FETCHING_TOP_NEWS,
  FETCHING_TOP_NEWS_SUCCESS,
  FETCHING_TOP_NEWS_FAILURE,
  FETCHING_TOP_NEWS_PER_CATEGORY,
  FETCHING_TOP_NEWS_PER_CATEGORY_SUCCESS,
  FETCHING_TOP_NEWS_PER_CATEGORY_FAILURE
} from "../NewsConstants";

jest.mock("utils/ApiCall");

describe("NewsActions tests", () => {
  describe("fetchAllTopNews tests", () => {
    it("Should dispatch loading - success", async () => {
      const stateMock = {
        [REDUCER_KEY]: {
          selectedCountry: {
            code: "GB",
            name: "Great Britain"
          }
        }
      };
      const dispatchMock = jest.fn();
      const getStateMock = jest.fn(() => stateMock);
      const mockedData = {
        articles: ["news 1", "news 2"],
        status: "ok",
        message: null
      };
      apiCall.mockImplementation(() => Promise.resolve(mockedData));
      await fetchAllTopNews("apple")(dispatchMock, getStateMock);

      expect(dispatchMock).toHaveBeenNthCalledWith(1, {
        type: FETCHING_TOP_NEWS
      });
      expect(dispatchMock).toHaveBeenNthCalledWith(2, {
        type: FETCHING_TOP_NEWS_SUCCESS,
        payload: mockedData.articles
      });
    });

    it("Should dispatch loading - error", async () => {
      const stateMock = {
        [REDUCER_KEY]: {
          selectedCountry: {
            code: "GB",
            name: "Great Britain"
          }
        }
      };
      const dispatchMock = jest.fn();
      const getStateMock = jest.fn(() => stateMock);
      const mockedData = {
        articles: [],
        status: "not OK",
        message: "Something bad happened"
      };
      apiCall.mockImplementation(() => Promise.resolve(mockedData));
      await fetchAllTopNews("apple")(dispatchMock, getStateMock);

      expect(dispatchMock).toHaveBeenNthCalledWith(1, {
        type: FETCHING_TOP_NEWS
      });
      expect(dispatchMock).toHaveBeenNthCalledWith(2, {
        type: FETCHING_TOP_NEWS_FAILURE,
        payload: mockedData.message
      });
    });

    it("Should dispatch loading - error from api", async () => {
      const stateMock = {
        [REDUCER_KEY]: {
          selectedCountry: {
            code: "GB",
            name: "Great Britain"
          }
        }
      };
      const dispatchMock = jest.fn();
      const getStateMock = jest.fn(() => stateMock);
      const error =
        "You have made too many requests recently. Developer accounts are limited to 500 requests over a 24 hour period (250 requests available every 12 hours). Please upgrade to a paid plan if you need more requests.";

      apiCall.mockImplementation(() => Promise.reject(new Error(error)));
      await fetchAllTopNews("apple")(dispatchMock, getStateMock);

      expect(dispatchMock).toHaveBeenNthCalledWith(1, {
        type: FETCHING_TOP_NEWS
      });
      expect(dispatchMock).toHaveBeenNthCalledWith(2, {
        type: FETCHING_TOP_NEWS_FAILURE,
        payload: error
      });
    });
  });

  describe("fetchTopNewsByCategory tests", () => {
    it("Should dispatch loading - success", async () => {
      const category = "business";
      const stateMock = {
        [REDUCER_KEY]: {
          selectedCountry: {
            code: "GB",
            name: "Great Britain"
          }
        }
      };
      const dispatchMock = jest.fn();
      const getStateMock = jest.fn(() => stateMock);
      const mockedData = {
        articles: ["news 1", "news 2"],
        status: "ok",
        message: null
      };
      apiCall.mockImplementation(() => Promise.resolve(mockedData));
      await fetchTopNewsByCategory(category, 25)(dispatchMock, getStateMock);

      expect(dispatchMock).toHaveBeenNthCalledWith(1, {
        type: FETCHING_TOP_NEWS_PER_CATEGORY,
        payload: category
      });
      expect(dispatchMock).toHaveBeenNthCalledWith(2, {
        type: FETCHING_TOP_NEWS_PER_CATEGORY_SUCCESS,
        payload: { news: mockedData.articles, category }
      });
    });

    it("Should dispatch loading - error", async () => {
      const category = "business";
      const stateMock = {
        [REDUCER_KEY]: {
          selectedCountry: {
            code: "GB",
            name: "Great Britain"
          }
        }
      };
      const dispatchMock = jest.fn();
      const getStateMock = jest.fn(() => stateMock);
      const mockedData = {
        articles: null,
        status: "not ok",
        message: "An error happened"
      };
      apiCall.mockImplementation(() => Promise.resolve(mockedData));
      await fetchTopNewsByCategory(category, 25)(dispatchMock, getStateMock);

      expect(dispatchMock).toHaveBeenNthCalledWith(1, {
        type: FETCHING_TOP_NEWS_PER_CATEGORY,
        payload: category
      });
      expect(dispatchMock).toHaveBeenNthCalledWith(2, {
        type: FETCHING_TOP_NEWS_PER_CATEGORY_FAILURE,
        payload: { error: mockedData.message, category }
      });
    });

    it("Should dispatch loading - error from api", async () => {
      const category = "business";
      const stateMock = {
        [REDUCER_KEY]: {
          selectedCountry: {
            code: "GB",
            name: "Great Britain"
          }
        }
      };
      const error =
        "You have made too many requests recently. Developer accounts are limited to 500 requests over a 24 hour period (250 requests available every 12 hours). Please upgrade to a paid plan if you need more requests.";

      const dispatchMock = jest.fn();
      const getStateMock = jest.fn(() => stateMock);

      apiCall.mockImplementation(() => Promise.reject(new Error(error)));
      await fetchTopNewsByCategory(category, 25)(dispatchMock, getStateMock);

      expect(dispatchMock).toHaveBeenNthCalledWith(1, {
        type: FETCHING_TOP_NEWS_PER_CATEGORY,
        payload: category
      });
      expect(dispatchMock).toHaveBeenNthCalledWith(2, {
        type: FETCHING_TOP_NEWS_PER_CATEGORY_FAILURE,
        payload: { error, category }
      });
    });
  });
});
