import NewsReducer from "../NewsReducer";
import {
  FETCHING_TOP_NEWS,
  INIT_STATE,
  FETCHING_TOP_NEWS_SUCCESS,
  FETCHING_TOP_NEWS_FAILURE,
  FETCHING_TOP_NEWS_PER_CATEGORY,
  FETCHING_TOP_NEWS_PER_CATEGORY_SUCCESS,
  FETCHING_TOP_NEWS_PER_CATEGORY_FAILURE
} from "../NewsConstants";

describe("NewsReducer tests", () => {
  it("Should be a function", () => {
    expect(typeof NewsReducer).toBe("function");
  });

  it("Should return an object", () => {
    expect(typeof NewsReducer({}, {})).toBe("object");
  });

  it("Should return loading top news", () => {
    const action = { type: FETCHING_TOP_NEWS };

    const result = NewsReducer(INIT_STATE, action);

    expect(result).toEqual({
      ...INIT_STATE,
      topNews: {
        loading: true
      }
    });
  });

  it("Should return top news", () => {
    const action = {
      type: FETCHING_TOP_NEWS_SUCCESS,
      payload: ["news 1", "news 2"]
    };

    const result = NewsReducer(INIT_STATE, action);

    expect(result).toEqual({
      ...INIT_STATE,
      topNews: {
        loading: false,
        news: action.payload
      }
    });
  });

  it("Should return top news error", () => {
    const action = {
      type: FETCHING_TOP_NEWS_FAILURE,
      payload: "An error happened"
    };

    const result = NewsReducer(INIT_STATE, action);

    expect(result).toEqual({
      ...INIT_STATE,
      topNews: {
        loading: false,
        error: action.payload
      }
    });
  });

  it("Should return top news per category loading", () => {
    const category = "business";
    const action = {
      type: FETCHING_TOP_NEWS_PER_CATEGORY,
      payload: category
    };

    const result = NewsReducer(INIT_STATE, action);

    expect(result).toEqual({
      ...INIT_STATE,
      newsPerCategory: {
        ...INIT_STATE.newsPerCategory,
        [category]: {
          loading: true
        }
      }
    });
  });

  it("Should return top news per category", () => {
    const category = "business";
    const action = {
      type: FETCHING_TOP_NEWS_PER_CATEGORY_SUCCESS,
      payload: {
        category,
        news: ["News 1", "News 2"]
      }
    };

    const result = NewsReducer(INIT_STATE, action);

    expect(result).toEqual({
      ...INIT_STATE,
      newsPerCategory: {
        ...INIT_STATE.newsPerCategory,
        [category]: {
          loading: false,
          news: action.payload.news
        }
      }
    });
  });

  it("Should return top news per category error", () => {
    const category = "business";
    const action = {
      type: FETCHING_TOP_NEWS_PER_CATEGORY_FAILURE,
      payload: {
        category,
        error: "Something bad happened"
      }
    };

    const result = NewsReducer(INIT_STATE, action);

    expect(result).toEqual({
      ...INIT_STATE,
      newsPerCategory: {
        ...INIT_STATE.newsPerCategory,
        [category]: {
          loading: false,
          error: action.payload.error
        }
      }
    });
  });
});
