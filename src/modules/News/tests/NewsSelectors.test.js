import {
  getAllTopNews,
  getAllTopNewsError,
  getAllTopNewsLoading,
  getAllNewsCategories,
  getNewsPerCategoryError,
  getNewsPerCategoryLoading,
  getNewsByCategory
} from "../NewsSelectors";
import { REDUCER_KEY } from "../NewsConstants";

describe("NewsSelectors tests", () => {
  describe("getAllTopNews select tests", () => {
    it("Should be a function", () => {
      expect(typeof getAllTopNews).toEqual("function");
    });

    it("should return top news from state", () => {
      const mockedData = {
        news: [
          {
            value: "test"
          }
        ],
        error: null,
        loading: true
      };

      const state = {
        [REDUCER_KEY]: {
          topNews: mockedData
        }
      };

      const result = getAllTopNews(state);

      expect(result).toEqual(mockedData.news);
    });
  });

  describe("getAllTopNewsError select tests", () => {
    it("Should be a function", () => {
      expect(typeof getAllTopNewsError).toEqual("function");
    });

    it("should return top news error from state", () => {
      const mockedData = {
        news: [],
        error: "Something bad happened",
        loading: true
      };

      const state = {
        [REDUCER_KEY]: {
          topNews: mockedData
        }
      };

      const result = getAllTopNewsError(state);

      expect(result).toEqual(mockedData.error);
    });
  });

  describe("getAllTopNewsLoading select tests", () => {
    it("Should be a function", () => {
      expect(typeof getAllTopNewsLoading).toEqual("function");
    });

    it("should return top news loading from state", () => {
      const mockedData = {
        news: [],
        error: null,
        loading: true
      };

      const state = {
        [REDUCER_KEY]: {
          topNews: mockedData
        }
      };

      const result = getAllTopNewsLoading(state);

      expect(result).toEqual(mockedData.loading);
    });
  });

  describe("getAllNewsCategories select tests", () => {
    it("Should be a function", () => {
      expect(typeof getAllNewsCategories).toEqual("function");
    });

    it("should return all news per category from state", () => {
      const initNewsState = {
        news: [],
        error: null,
        loading: true
      };

      const mockedState = {
        business: {
          ...initNewsState
        },
        entertainment: {
          ...initNewsState
        },
        general: {
          ...initNewsState
        },
        health: {
          ...initNewsState
        },
        science: {
          ...initNewsState
        },
        sports: {
          ...initNewsState
        },
        technology: {
          ...initNewsState
        }
      };

      const state = {
        [REDUCER_KEY]: {
          newsPerCategory: mockedState
        }
      };

      const result = getAllNewsCategories(state);

      expect(result).toEqual(Object.keys(mockedState));
    });
  });

  describe("getNewsByCategory select tests", () => {
    it("Should be a function", () => {
      expect(typeof getNewsByCategory).toEqual("function");
    });

    it("Should return news by category", () => {
      const category = "business";
      const mockedData = {
        news: [
          {
            value: "test"
          }
        ],
        error: null,
        loading: true
      };

      const mockedState = {
        [REDUCER_KEY]: {
          newsPerCategory: {
            [category]: mockedData
          }
        }
      };

      const result = getNewsByCategory(mockedState, category);

      expect(result).toEqual(mockedData.news);
    });
  });

  describe("getNewsPerCategoryError select tests", () => {
    it("Should be a function", () => {
      expect(typeof getNewsPerCategoryError).toEqual("function");
    });

    it("Should return news error by category", () => {
      const category = "business";
      const mockedData = {
        news: [],
        error: "An error happened",
        loading: true
      };

      const mockedState = {
        [REDUCER_KEY]: {
          newsPerCategory: {
            [category]: mockedData
          }
        }
      };

      const result = getNewsPerCategoryError(mockedState, category);

      expect(result).toEqual(mockedData.error);
    });
  });

  describe("getNewsPerCategoryLoading select tests", () => {
    it("Should be a function", () => {
      expect(typeof getNewsPerCategoryLoading).toEqual("function");
    });

    it("Should return news loading by category", () => {
      const category = "business";
      const mockedData = {
        news: [],
        error: null,
        loading: false
      };

      const mockedState = {
        [REDUCER_KEY]: {
          newsPerCategory: {
            [category]: mockedData
          }
        }
      };

      const result = getNewsPerCategoryLoading(mockedState, category);

      expect(result).toEqual(mockedData.loading);
    });
  });
});
