import React from "react";
import { NavLink } from "react-router-dom";

import CountrySelection from "components/CountrySelection";

import styles from "./Navbar.module.css";

const Navbar = () => (
  <div className={styles.container}>
    <div>
      <NavLink
        className={styles.link}
        activeClassName={styles.linkActive}
        to="/"
        exact
      >
        Top News
      </NavLink>
      <NavLink
        className={styles.link}
        activeClassName={styles.linkActive}
        to="/categories"
      >
        Categories
      </NavLink>
      <NavLink
        className={styles.link}
        activeClassName={styles.linkActive}
        to="/search"
      >
        Search
      </NavLink>
    </div>
    <div>
      <CountrySelection />
    </div>
  </div>
);

export default Navbar;
