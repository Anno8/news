import React from "react";
import { Switch, Route } from "react-router-dom";

import TopNewsPage from "pages/TopNewsPage";
import SearchPage from "pages/SearchPage";
import CategoriesPage from "pages/CategoriesPage";
import NotFoundPage from "pages/NotFoundPage";
import NewsDetailsPage from "pages/NewsDetailsPage";

import styles from "./AppRoutes.module.css";

const App = () => (
  <div className={styles.container}>
    <Switch>
      <Route path="/" exact component={TopNewsPage} />
      <Route path="/search" exact component={SearchPage} />
      <Route path="/categories" exact component={CategoriesPage} />
      <Route path="/newsDetails" exact component={NewsDetailsPage} />
      <Route path="*" component={NotFoundPage} />
    </Switch>
  </div>
);

export default App;
