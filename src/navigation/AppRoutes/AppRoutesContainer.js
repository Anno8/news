import { connect } from "react-redux";

import AppRoutes from "./AppRoutes";

export default connect()(AppRoutes);
