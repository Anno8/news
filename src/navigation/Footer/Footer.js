import React from "react";

import styles from "./Footer.module.css";

const Footer = () => (
  <div className={styles.container}>
    <a href="https://newsapi.org">Powered by News API</a>
  </div>
);

export default Footer;
