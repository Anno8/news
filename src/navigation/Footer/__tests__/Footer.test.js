import { shallow } from "enzyme";
import React from "react";

import Footer from "../Footer";

describe("Footer component tests", () => {
  it("Should match snapshot", () => {
    const wrapper = shallow(<Footer />);

    expect(wrapper).toMatchSnapshot();
  });
});
