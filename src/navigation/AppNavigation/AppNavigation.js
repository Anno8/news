import React from "react";
import { BrowserRouter } from "react-router-dom";

import Navbar from "navigation/Navbar";
import AppRoutes from "navigation/AppRoutes";
import Footer from "navigation/Footer";

import styles from "./AppNavigation.module.css";

const AppNavigation = () => (
  <BrowserRouter>
    <div className={styles.container}>
      <Navbar />
      <AppRoutes />
      <Footer />
    </div>
  </BrowserRouter>
);

export default AppNavigation;
