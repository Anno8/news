import React from "react";

import { shallow } from "enzyme";

import AppNavigation from "../AppNavigation";

describe("AppNavigation tests", () => {
  it("Should match snapshot", () => {
    const wrapper = shallow(<AppNavigation />);
    expect(wrapper).toMatchSnapshot();
  });
});
