import { shallow } from "enzyme";
import React from "react";

import { NewsCard } from "../NewsCard";

describe("NewsCard component tests", () => {
  const mockedProps = {
    history: {
      push: jest.fn()
    },
    news: {
      title: "test title",
      description: "test description",
      urlToImage: "test urlToImage",
      content: "test content"
    }
  };
  it("Should match snapshot", () => {
    const wrapper = shallow(<NewsCard {...mockedProps} />);

    expect(wrapper).toMatchSnapshot();
  });

  it("Should navigate to url on more", () => {
    const wrapper = shallow(<NewsCard {...mockedProps} />);
    const more = wrapper.find(".link");

    more.simulate("click");

    expect(mockedProps.history.push).toHaveBeenCalledWith("newsDetails", {
      content: mockedProps.news.content,
      title: mockedProps.news.title,
      urlToImage: mockedProps.news.urlToImage
    });
  });
});
