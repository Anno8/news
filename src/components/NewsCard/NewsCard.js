import React from "react";

import { withRouter } from "react-router-dom";

import styles from "./NewsCard.module.css";

export const NewsCard = ({
  history,
  news: { title, description, urlToImage, content }
}) => (
  <div className={styles.container}>
    <h4>{title}</h4>
    <img className={styles.image} src={urlToImage} alt={title} />
    <p>{description}</p>
    <p
      className="link"
      onClick={() =>
        history.push("newsDetails", { title, content, urlToImage })
      }
    >
      More &gt;
    </p>
  </div>
);

export default withRouter(NewsCard);
