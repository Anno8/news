import { connect } from "react-redux";

import {
  getSelectedCountry,
  getCountries
} from "modules/Country/CountrySelectors";
import { changeCountry } from "modules/Country/CountryActions";

import CountrySelection from "./CountrySelection";

export const mapStateToProps = (state) => ({
  selectedCountry: getSelectedCountry(state),
  countries: getCountries(state)
});

export default connect(mapStateToProps, { changeCountry })(CountrySelection);
