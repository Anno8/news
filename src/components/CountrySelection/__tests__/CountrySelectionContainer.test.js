import { mapStateToProps } from "../CountrySelectionContainer";

jest.mock("modules/Country/CountrySelectors", () => ({
  getSelectedCountry: jest.fn(() => "selected country"),
  getCountries: jest.fn(() => "countries")
}));
describe("CountrySelectionContainer tests", () => {
  it("Should be a function", () => {
    expect(typeof mapStateToProps).toEqual("function");
  });

  it("should return an object", () => {
    expect(typeof mapStateToProps({})).toEqual("object");
  });

  it("should have property values", () => {
    const props = mapStateToProps({});
    expect(props).toHaveProperty("selectedCountry");
    expect(props).toHaveProperty("countries");
  });
});
