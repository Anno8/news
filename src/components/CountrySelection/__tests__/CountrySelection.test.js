import { shallow } from "enzyme";
import React from "react";

import CountrySelection from "../CountrySelection";

describe("CountrySelection component tests", () => {
  const mockedProps = {
    selectedCountry: {
      code: "US",
      name: "United states"
    },
    countries: [
      {
        code: "GB",
        name: "Great Britain"
      },
      {
        code: "US",
        name: "United States"
      }
    ],
    changeCountry: jest.fn()
  };
  it("Should match snapshot", () => {
    const wrapper = shallow(<CountrySelection {...mockedProps} />);

    expect(wrapper).toMatchSnapshot();
  });

  it("Should call changeCountry on click", () => {
    const wrapper = shallow(<CountrySelection {...mockedProps} />);
    const button = wrapper.find("button").at(0);

    button.simulate("click");

    expect(mockedProps.changeCountry).toHaveBeenCalledWith(
      mockedProps.countries[0]
    );
  });
});
