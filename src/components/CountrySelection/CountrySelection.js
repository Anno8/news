import React from "react";

export const CountrySelection = ({
  selectedCountry,
  countries,
  changeCountry
}) => (
  <div>
    {countries.map((c, index) => (
      <button
        key={index}
        onClick={() => changeCountry(c)}
        disabled={c.code === selectedCountry.code}
        className="countrySelection_button"
      >
        {c.code}
      </button>
    ))}
  </div>
);

export default CountrySelection;
