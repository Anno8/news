import { shallow } from "enzyme";
import React from "react";

import CategoriesDisplay from "../CategoriesDisplay";

describe("CategoriesDisplay tests", () => {
  const mockedProps = {
    fetchTopNewsByCategory: jest.fn(),
    news: [
      {
        title: "News 1"
      },
      {
        title: "News 2"
      }
    ],
    category: "business",
    error: null,
    loading: false,
    country: {
      code: "US",
      name: "United States"
    }
  };
  it("Should match snapshot", () => {
    const wrapper = shallow(<CategoriesDisplay {...mockedProps} />);

    expect(wrapper).toMatchSnapshot();
  });

  it("Should match snapshot when loading", () => {
    const wrapper = shallow(<CategoriesDisplay {...mockedProps} loading />);

    expect(wrapper).toMatchSnapshot();
  });

  it("Should match snapshot when an error ocurred", () => {
    const wrapper = shallow(
      <CategoriesDisplay {...mockedProps} error="An error happened" />
    );

    expect(wrapper).toMatchSnapshot();
  });
});
