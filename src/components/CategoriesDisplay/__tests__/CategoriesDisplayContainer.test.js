import { mapStateToProps } from "../CategoriesDisplayContainer";

jest.mock("modules/Country/CountrySelectors", () => ({
  getSelectedCountry: jest.fn(() => "selected country")
}));

jest.mock("modules/News/NewsSelectors", () => ({
  getNewsByCategory: jest.fn(() => ["News1", "News2"]),
  getNewsPerCategoryError: jest.fn(() => null),
  getNewsPerCategoryLoading: jest.fn(() => false)
}));
describe("TopNewsPageContainer tests", () => {
  it("Should be a function", () => {
    expect(typeof mapStateToProps).toEqual("function");
  });

  it("should return an object", () => {
    expect(typeof mapStateToProps({}, { category: "business" })).toEqual(
      "object"
    );
  });

  it("should have property values", () => {
    const props = mapStateToProps({}, { category: "business" });
    expect(props).toHaveProperty("news");
    expect(props).toHaveProperty("error");
    expect(props).toHaveProperty("loading");
    expect(props).toHaveProperty("country");
  });
});
