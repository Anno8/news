import React, { useState, useEffect } from "react";
import Carousel from "react-multi-carousel";
import Expand from "react-expand-animated";

import NewsCard from "components/NewsCard";
import "react-multi-carousel/lib/styles.css";
import Loader from "components/Loader";

import styles from "./CategoriesDisplay.module.css";

const responsive = {
  superLargeDesktop: {
    breakpoint: { max: 4000, min: 3000 },
    items: 5
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1
  }
};

const CategoriesDisplay = ({
  category,
  fetchTopNewsByCategory,
  error,
  news,
  country,
  loading
}) => {
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    // paging 5 - all
    fetchTopNewsByCategory(category);
  }, [fetchTopNewsByCategory, category, country]);
  return (
    <div>
      <button
        onClick={() => {
          setIsOpen(!isOpen);
        }}
        className={`${styles.accordion} ${isOpen && styles.active}`}
      >
        {isOpen ? `Top ${category} news from ${country.name}` : category}
      </button>

      {loading && <Loader />}
      {error && <div>{error}</div>}
      <Expand
        open={isOpen}
        duration={500}
        transitions={["height"]}
        easing="ease-in-out"
      >
        <div className={styles.container}>
          {!loading &&
            !error &&
            news.map((n) => <NewsCard key={n.title} news={n} />)}
        </div>
      </Expand>
      {!isOpen && (
        <Carousel swipeable={true} responsive={responsive}>
          {!loading &&
            !error &&
            news.slice(0, 5).map((n) => <NewsCard key={n.title} news={n} />)}
        </Carousel>
      )}
    </div>
  );
};

export default CategoriesDisplay;
