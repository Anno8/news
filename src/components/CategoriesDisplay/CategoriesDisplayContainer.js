import { connect } from "react-redux";

import {
  getNewsByCategory,
  getNewsPerCategoryLoading,
  getNewsPerCategoryError
} from "modules/News/NewsSelectors";
import { fetchTopNewsByCategory } from "modules/News/NewsActions";

import { getSelectedCountry } from "modules/Country/CountrySelectors";

import CategoriesDisplay from "./CategoriesDisplay";

export const mapStateToProps = (state, { category }) => ({
  news: getNewsByCategory(state, category),
  loading: getNewsPerCategoryLoading(state, category),
  error: getNewsPerCategoryError(state, category),
  country: getSelectedCountry(state)
});

export default connect(mapStateToProps, { fetchTopNewsByCategory })(
  CategoriesDisplay
);
