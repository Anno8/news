import { apiUrl, apiKey } from "config/constants";

// Centralized place for all api calls.
// this way if the need ever arises to replace it with, for example axios
// the change could be made here and consumers would stay the same
export const apiCall = async (url) =>
  await fetch(`${apiUrl}/${url}`, {
    headers: {
      "X-Api-Key": apiKey
    }
  }).then((response) => response.json());
