import { apiKey, apiUrl } from "config/constants";

import { apiCall } from "../ApiCall";

describe("ApiCall tests", () => {
  it("Should call fetch with provided parameters", async () => {
    const testUrl = "Something";
    const jsonMock = {
      value: "test"
    };

    jest.spyOn(global, "fetch").mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(jsonMock),
        status: 200
      })
    );
    await apiCall(testUrl);

    expect(fetch).toBeCalledWith(`${apiUrl}/${testUrl}`, {
      headers: {
        "X-Api-Key": apiKey
      }
    });
  });
});
