import { REDUCER_KEY as countryReducerKey } from "modules/Country/CountryConstants";
import countryReducer from "modules/Country/CountryReducer";

import { REDUCER_KEY as newsReducerKey } from "modules/News/NewsConstants";
import newsReducer from "modules/News/NewsReducer";

export default {
  [countryReducerKey]: countryReducer,
  [newsReducerKey]: newsReducer
};
